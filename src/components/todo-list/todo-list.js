import React from 'react';
import TodoListItem from '../todo-list-item/';
import './todo-list.css';

const TodoList = ({ todos, onDeleted, onToggleImportant, onToggleDone }) => {
  const todoListElements = todos.map(({ id, ...itemProps }) => (
    <li key={id} className="list-group-item">
      <TodoListItem
        {...itemProps}
        onDeleted={() => onDeleted(id)}
        onToggleImportant={() => onToggleImportant(id)}
        onToggleDone={() => onToggleDone(id)}
      />
    </li>
  ));

  return <ul className="list-group todo-list">{todoListElements}</ul>;
};

export default TodoList;
