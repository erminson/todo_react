import React, { Component } from 'react';

import AppHeader from '../app-header/';
import SearchPanel from '../search-panel/';
import ItemStatusFilter from '../item-status-filter/';
import TodoList from '../todo-list/';
import AddItemForm from '../add-item-form';

import './app.css';

export default class App extends Component {
  createTodoItem(id, label, important = false, done = false) {
    return { id, label, important, done };
  }

  state = {
    todos: [
      this.createTodoItem(1, 'Learn React', false, true),
      this.createTodoItem(2, 'Build Awesome React App', true),
      this.createTodoItem(3, 'Have a lunch'),
    ],
    term: '',
    filter: 'all', // all, done, active
  };

  findIndexById = (todos, id) => {
    return todos.findIndex(({ id: currId }) => currId === id);
  };

  onDeleteTodo = (id) => {
    this.setState(({ todos }) => {
      const index = this.findIndexById(todos, id);
      const newTodos = [...todos];
      newTodos.splice(index, 1);
      return {
        todos: newTodos,
      };
    });
  };

  onAddTodo = (label) => {
    const newId = this.state.todos.reduce((max, { id }) => Math.max(max, id), -Infinity) + 1;
    const newTodo = this.createTodoItem(newId, label);
    this.setState(({ todos }) => {
      const newTodos = [...todos, newTodo];
      return {
        todos: newTodos,
      };
    });
  };

  toggleProperty(arr, id, propName) {
    const index = this.findIndexById(arr, id);

    const oldItem = arr[index];
    const newItem = { ...oldItem, [propName]: !oldItem[propName] };

    return [...arr.slice(0, index), newItem, ...arr.slice(index + 1)];
  }

  onToggleImportant = (id) => {
    this.setState(({ todos }) => {
      const newTodos = this.toggleProperty(todos, id, 'important');
      return {
        todos: newTodos,
      };
    });
  };

  onToggleDone = (id) => {
    this.setState(({ todos }) => {
      const newTodos = this.toggleProperty(todos, id, 'done');
      return {
        todos: newTodos,
      };
    });
    console.log(`On Toggle Done: ${id}`);
  };

  onSearch = (term) => {
    this.setState({ term });
  };

  onFilter = (filter) => {
    this.setState({ filter });
  };

  search(todos, term) {
    return todos.filter(({ label }) => label.toLowerCase().includes(term.toLowerCase().trim()));
  }

  filter(todos, pred) {
    switch (pred) {
      case 'all':
        return todos;
      case 'active':
        return todos.filter(({ done }) => !done);
      case 'done':
        return todos.filter(({ done }) => done);
      default:
        return todos;
    }
  }

  render() {
    const { todos, term, filter } = this.state;
    const visibleTodos = this.filter(this.search(todos, term), filter);
    const doneCount = todos.filter(({ done }) => done).length;
    const todoCount = todos.length - doneCount;
    return (
      <div className="todo-app">
        <AppHeader toDo={todoCount} done={doneCount} />
        <div className="top-panel d-flex">
          <SearchPanel onSearch={this.onSearch} />
          <ItemStatusFilter filter={filter} onFilterChange={this.onFilter} />
        </div>
        <TodoList
          todos={visibleTodos}
          onDeleted={this.onDeleteTodo}
          onToggleImportant={this.onToggleImportant}
          onToggleDone={this.onToggleDone}
        />
        <AddItemForm onAddTodo={this.onAddTodo} />
      </div>
    );
  }
}
