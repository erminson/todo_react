import React, { Component } from 'react';

import './item-status-filter';

export default class ItemStatusFilter extends Component {
  buttons = [
    { name: 'all', label: 'All' },
    { name: 'active', label: 'Active' },
    { name: 'done', label: 'done' },
  ];
  render() {
    const { filter, onFilterChange } = this.props;
    const buttonElements = this.buttons.map(({ name, label }) => {
      const isActive = name === filter;
      const clazz = isActive ? 'btn-info' : 'btn-outline-secondary';
      return (
        <button key={name} type="button" className={`btn ${clazz}`} onClick={() => onFilterChange(name)}>
          {label}
        </button>
      );
    });
    return <div className="btn-group">{buttonElements}</div>;
  }
}
