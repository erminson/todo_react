import React, { Component } from 'react';

import './add-item-form.css';

export default class AddItemForm extends Component {
  state = {
    label: '',
  };

  onLabelChange = (event) => {
    this.setState({
      label: event.target.value,
    });
  };

  onSubmit = (event) => {
    event.preventDefault();
    this.props.onAddTodo(this.state.label);
    this.setState({ label: '' });
  };

  render() {
    return (
      <form className="item-add-form d-flex" onSubmit={this.onSubmit}>
        <input
          type="text"
          className="form-control"
          onChange={this.onLabelChange}
          placeholder="What needs to be done"
          value={this.state.label}
        />
        <button type="button" className="btn btn-outline-secondary">
          Add Item
        </button>
      </form>
    );
  }
}
